package com.baomidou.crab.v1.mapper;

import com.baomidou.crab.v1.entity.Tag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标签表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-02-07
 */
public interface TagMapper extends BaseMapper<Tag> {

}
